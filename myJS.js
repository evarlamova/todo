$(() => {
    let todoList = [];
    let mycheck = 0 ;
    let mypag = 1;

    // АКТИВНОСТЬ ВКЛАДОК
    $('.switch').on('click',function activeTask(){
        $('.switch').css('backgroundColor','none');
        $('.switch').css('color', 'none');

        $(this).css('backgroundColor','grey' );
        $(this).css('color', 'white');
    });

    // ДОБАВЛЕНИЕ НОВОГО
    $('#add_button').on('click', function () {
        let todo = $('#add_input').val();
        if (!todo || todo.trim()==='') {
            alert("you must solve real tasks...");
        }
        else {
            let temp = {};
            temp.todo = todo;
            temp.check = false;
            temp.id = Math.random();

            let i = todoList.length;
            todoList[i] = temp;

            out(todoList);
            delete_add();
        }

        // localStorage.setItem('todo', JSON.stringify(todoList));
        // не робит
        // localStorage.setItem('check',  JSON.stringify(todoList));

    });

    // ВЫБОР БОЛЬШОГО ЧЕКБОКСА, КОГДА ВСЕ ВЫБРАНО
    $(document).on ('change', function () {
        let k=0;
        const l =todoList.length;
        $('#choose_all').prop('checked',false);
        for(let i = 0; i< todoList.length; i++) {
            if (todoList[i].check ===true) {
                k ++;
            }
        }
        if (k === (todoList.length) && todoList.length !== 0 ) {
            $('#choose_all').prop('checked',true);
        }
       else{
            $('#choose_all').prop('checked',false);
        }
    })

    // ВЫПОЛНЕНИЕ ТАСКА (checkbox)
    $('#out_added').on('change', '.check-todo', function () {
        const idForCheck = Number($(this).parent().attr('id'));
        todoList.forEach(item => {
            if (item.id === idForCheck) {
                item.check = !item.check;
            }
        });
        out(todoList);
        // localStorage.setItem('check', JSON.stringify(todoList));
    });

    // УДАЛЕНИЕ ОДНОГО ЭЛЕМЕНТА
    $('#out_added').on('click', '.del-todo', function () {
        const a = $(this).parent().attr('id');
        todoList.forEach((item, index)=> {
            if (a === item.id) {
                todoList.splice(index,1);
            }

            if ($.isEmptyObject(todoList)) {
                $('#out_added').html('there will be tasks...');
                 }
            else { out(todoList)}
        })
    });

    // РЕДАКТИРОВАНИЕ ПО ДВОЙНОМУ НАЖАТИЮ
    $(document).on('dblclick', '.task-todo', function () {
        let todo_new = $(this).parent().attr('todo');
        // тут цикл проверки по ид
        let todo = $(this);
        // let inputEdit = $('.input-task-edit');

        let inputEdit = $('.myinput');

        $('.myinput').css('display', 'table');
        $('.task-todo').css('display', 'none');
        $('.check-todo').css('display', 'none');
        $('.del-todo').css('display', 'none');


        const a = $(this).parent().attr('id');
        // $('#myModal').modal("show");
        inputEdit.val(todo.text());
        // $('.save-changes').on('click', ()=> {
        $('.myinput').on('change', ()=> {
            // val(todo_new);
            todoList.forEach((item, index)=> {
                // console.log(+item.id);
                // console.log(a);
                if (+a === +item.id) {
                    item.todo = inputEdit.val().trim() || todo.text();
                    out(todoList);
                }
            })
        });
        delete_add();
    });

    // ПОКАЗАТЬ ВСЕ
    $(document).on('click', '#show_all', function () {
        mycheck = 0 ;
        out(todoList);
    });

    // ВЫБРАТЬ ВСЕ
    $(document).on('change', '#choose_all', function () {
    // проверяет состояние нажатости на наш  чекбокс (тру фолс)
    let a = $(this).is(":checked") ;

        if (a === true) {
            for (let i = 0; i < todoList.length; i++) {
                todoList[i].check = true;
            }
        }
        else if (a  === false) {
            for (let i = 0; i < todoList.length; i++) {
                todoList[i].check = false;
            }
        }
        else {alert("my bad, sorry")}
        out(todoList);
    });

    // ПОКАЗАТЬ АКТИВНЫЕ
    $(document).on('click', '#show_true', function () {
        mycheck = 1;
        out(todoList);
    });

    // ПОКАЗАТЬ ЗАВЕРШЕННЫЕ
    $(document).on('click', '#show_false', function () {
        mycheck = 2;
        out(todoList);
    });

    // УДАЛЕНИЕ ОТМЕЧЕННЫХ
    $(document).on('click', '#del_true', function () {
        todoList =todoList.filter(item=>{
            return item.check===false;
        });
        out(todoList);
        if ($.isEmptyObject(todoList)) {
            $('#out_added').html('there will be tasks ...');
        }
    });

    // СКРЫТЬ ПОКАЗАТЬ СОДЕРЖИМОЕ
    $('#hide').click(function() {
        $('#out_added').hide(1000);
    });
    $('#show').click(function() {
        $('#out_added').show(1000);
    });

    // OUT
    function out(todoList123) {
        let todoList1230 =[];
        let out = '';

        $("#out_added").empty();
        $("#massege_task").empty();

        // const all_task = todoList123.length;
        // let false_task = 0;
        // let true_task = 0;

        if (mycheck === 1) {
            todoList1230 =todoList.filter(item=>{
                return item.check===false;

            });
        }
        else if (mycheck ===2 ) {
            todoList1230 =todoList.filter(item=>{
                return item.check===true;
            });
        }
        else{
            todoList1230 = todoList123;
        }

        for (let key in todoList1230) {

            out = out + '<hr>' + `<span id="${todoList1230[key].id}"><input class="check-todo " type="checkbox" ${todoList1230[key].check ? 'checked' : ''}>` + '       ';
            out = ''+out + '<input class="myinput" id="'+todoList1230[key].id+'"><a class="task-todo">'+todoList1230[key].todo +'</a>'+ '</>' + `<span id="text${todoList1230[key].id}"><button type="button" class="close del-todo" aria-label="Close" " >\n <span aria-hidden="true">×</span>\n` +
                '</button></span>' + '<hr>';
            $("#out_added").html(out);
            $('.myinput').css('display', 'none')

        }

        todoList12301 = todoList1230;

        // вставлять ее до  пагинации?
        countTask(todoList123);

        // if ((!($.isEmptyObject(todoList123))) || todoList123 !== [] || todoList123 !== undefined || todoList123 !== null ) {
        //     for (let i = 0; i < all_task; i++) {
        //         if (todoList123[i].check === false) {
        //             false_task++;
        //         }
        //         if (todoList123[i].check === true) {
        //             true_task++;
        //         }
        //
        //         // счетчик выполненных задач
        //         if (false_task === undefined || false_task === 0 || false_task === null || (true_task === all_task && (all_task !== 0 || all_task !== [] || all_task !== null))) {
        //             $("#false_task").html('');
        //             $("#true_task").html('');
        //             $("#massege_task").html('\n' + 'all tasks done, great! day figure - ' + all_task);
        //         } else if (false_task === all_task || true_task === undefined || true_task === 0 || true_task === null) {
        //             $("#false_task").html('');
        //             $("#true_task").html('');
        //             $("#massege_task").html('\n' + 'nothing is done yet ... the figure on the horizon is ' + all_task);
        //
        //         } else {
        //             $("#false_task").html(false_task + '/' + all_task);
        //             $("#true_task").html(true_task + '/' + all_task);
        //             $("#massege_task").html('');
        //         }
        //
        //     }
        //
        // }










todoPagination(todoList1230);
        // console.log(todoList1230);

    }

    //счетчик
    // function countTask(todoList123) {
    //     const all_task = todoList123.length;
    //     let false_task = 0;
    //     let true_task = 0;
    //
    //     if ((!($.isEmptyObject(todoList123))) || todoList123 !== [] || todoList123 !== undefined || todoList123 !== null ) {
    //         for (let i = 0; i < all_task; i++) {
    //             if (todoList123[i].check === false) {
    //                 false_task++;
    //             }
    //             if (todoList123[i].check === true) {
    //                 true_task++;
    //             }
    //
    //             // счетчик выполненных задач
    //             if (false_task === undefined || false_task === 0 || false_task === null || (true_task === all_task && (all_task !== 0 || all_task !== [] || all_task !== null))) {
    //                 $("#false_task").html('');
    //                 $("#true_task").html('');
    //                 $("#massege_task").html('\n' + 'all tasks done, great! day figure - ' + all_task);
    //             } else if (false_task === all_task || true_task === undefined || true_task === 0 || true_task === null) {
    //                 $("#false_task").html('');
    //                 $("#true_task").html('');
    //                 $("#massege_task").html('\n' + 'nothing is done yet ... the figure on the horizon is ' + all_task);
    //
    //             } else {
    //                 $("#false_task").html(false_task + '/' + all_task);
    //                 $("#true_task").html(true_task + '/' + all_task);
    //                 $("#massege_task").html('');
    //             }
    //
    //         }
    //
    //     }
    // }







    // пагинация
    function todoPagination(todoListPag) {
        // console.log(todoListPag);

        let temp = [];
        let all_task = todoListPag.length;
        const taskOnPage = 3;
        let countPage = Math.ceil((all_task / taskOnPage));
        // && ($('#add_button').on('click' ))  ????
        if (((todoListPag.length % taskOnPage) === 1)) {
            // добавляет в конец
            $('#pagination').append('<a class="pageActive" name="' + countPage + '"> ' + countPage + '</a>');

            // делаем новый массив по текущей странице
            $('.pageActive').on('click', function () {
                // for( let i = 0;i  < taskOnPage; i++){
                // console.log(((this.name)))
                if ((this.name)) {
                    for (let j = 0; j < taskOnPage; j++) {
                        temp[j] = todoListPag[(((this.name) * taskOnPage) - taskOnPage + j)];
                        // console.log(temp[j])

                    }
                    $('#out_added').empty();
                    out(temp);
                    // console.log(temp)
                    // }
                }

            });

            // активность страниц
            $('.pageActive').on('click', function () {
                $('.pageActive').css('backgroundColor', 'none');
                $('.pageActive').css('color', 'none');

                $(this).css('backgroundColor', '#000');
                $(this).css('color', 'white');
            });
            // console.log(temp)
        }
    }







})

function delete_add() {
    // document.getElementById("add_input").value = null;
    $("#add_input").val(null);

}

$("#add_input").keypress(function (e) {
    if (e.which == 13) {
        $("#add_button").click();
        delete_add();
    }
});
$(".input-task-edit").keypress(function (e) {
    if (e.which == 13) {
        $(".save-changes").click();
    }
});

