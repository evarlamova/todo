let data = [];
let activePage;
let dataPage = [];
let page =0;
let arrayPage=[];
let mycheck = 0;
let todoList1 = [];

function out() {

    if (mycheck === 1)
    {
        todoList1 =data.filter(item=>{
            return item.check===false;
        });
        arrayPage = [];
        page = 0;

    }
    else if (mycheck === 2 )
    {
        todoList1 =data.filter(item=>{
            return item.check===true;
        });
        arrayPage = [];
        page = 0;
    }
    else
    {
        todoList1 = data;
    }

    dataPage = arrForPage(todoList1);
    if (dataPage.length !==0) {

        render(dataPage);
    }
    else render(todoList1);

    countTasks();
    if($.isEmptyObject(todoList1))
    {
        $('.out_added').html('there will be tasks ...');
        $('.pageActive').html('');
    }
}
function render(todoList) {

    let out = '';
    $("#out_added").empty();
    $("#massege_task").empty();

    todoList.forEach((item,index)=>{

        const idTodo = item.id;

        out = out + '<hr>' + `<span id="${idTodo}"><input class="check-todo " type="checkbox" ${item.check ? 'checked' : ''}>` + '       ';
        out = ''+out + '<a class="task-todo">'+item.todo +'</a>'+ '</>' + `<span id="${item.id}+todo"><button type="button" class="close del-todo" aria-label="Close" " >\n <span aria-hidden="true">×</span>\n` +
            '</button></span>' + '<hr>';

        if(index<5) {
            $("#out_added").html(out);
        }
    })

}
function countTasks() {

    const all_task = data.length;

    let false_task = 0;
    let true_task = 0;

    if ((!($.isEmptyObject(data))) || data !== [] || data !== undefined || data !== null )
    {
        for (let i = 0; i < all_task; i++)
        {
            if (data[i].check === false)
            {
                false_task++;
            }
            if (data[i].check === true)
            {
                true_task++;
            }

            // счетчик выполненных задач
            if (false_task === undefined || false_task === 0 || false_task === null || (true_task === all_task && (all_task !== 0 || all_task !== [] || all_task !== null)))
            {
                $("#false_task").html('');
                $("#true_task").html('');
                $("#massege_task").html('great! you made ' + all_task +' tasks');
            }
            else if (false_task === all_task || true_task === undefined || true_task === 0 || true_task === null)
            {
                $("#false_task").html('');
                $("#true_task").html('');
                $("#massege_task").html('not good...active ' + all_task+'tasks');
            }
            else
            {
                $("#false_task").html(false_task + '/' + all_task);
                $("#true_task").html(true_task + '/' + all_task);
                $("#massege_task").html('');
            }

        }

    }

}
function arrForPage(todoList) {

    let temp = [];

    for (let i = 0; i < 5; i++)
    {
        let x = todoList[((activePage * 5) - 5 + i)];
        if (x) temp.push(x);
    }
    return temp;
}
function pagination(){
    const dataLen = todoList1.length;
    let countPage = Math.ceil((dataLen/5));
    if (page !== countPage )
    {
        arrayPage=[];
        page = countPage;
        for( let i =0 ; i < page; i++)
        {
            arrayPage[i]=i+1;
        }
        $('#pagination').html('');

        arrayPage.map(i => {
            $('#pagination').append('<a class="pageActive" id="'+i+'"> '+ i+'</a>');
        })

    }

    $('.pageActive').on('click',function(){
        activePage = this.id;
        out();

        $('.pageActive').css('backgroundColor','none');
        $('.pageActive').css('color', 'none');
        $(this).css('backgroundColor','#000' );
        $(this).css('color', 'white');
    });


    out();
}

function checkbox () {
    let k=0;
    for(let i = 0; i< data.length; i++)
    {
        if (data[i].check ===true)
        {
            k ++;
        }
    }
    if (k === (data.length) && data.length !== 0 )
    {
        $('#choose_all').prop('checked',true);
    }
    else
    {
        $('#choose_all').prop('checked',false);
    }
}
function delete_add() {
    $("#add_input").val(null);
}
function htmlspecialchars(html) {
    html = html.replace(/&/g, "&amp;");
    html = html.replace(/</g, "&lt;");
    html = html.replace(/>/g, "&gt;");
    html = html.replace(/"/g, "&quot;");
    return html;
}

$(()=>{

    // ДОБАВЛЕНИЕ НОВОГО
    $('#add_button').on('click', function () {
        let todo = htmlspecialchars($('#add_input').val());
        if (!todo || !todo.trim())
        {
            alert("you must solve real tasks...");
        }
        else
        {
            let temp = {};
            temp.todo = todo;
            temp.check = false;
            temp.id = Math.random();

            data.push(temp);

        }

        out();
        checkbox();
        pagination();
        delete_add();
    });

    // СМЕНА СОСТОЯНИЕ ЧЕКБОКСА, ВЫБРАТЬ ВСЕ, ИЗМЕНЕНИЕ СОСТОЯНИЕ БОЛЬШОГО ЧЕКБОКСА В ЗАВИСИМОСТИ ОТ СОСТ МАЛЕНЬКИХ
    $('#out_added').on('change', '.check-todo', function () {
        const idForCheck = Number($(this).parent().attr('id'));
        data.forEach(item => {
            if (item.id === idForCheck)
            {
                item.check = !item.check;
            }
        });
        out();
        checkbox();
        pagination();
    });
    $(document).on('change', '#choose_all', function () {
        let a = $(this).is(":checked") ;

        if (a === true)
        {
            for (let i = 0; i < data.length; i++)
            {
                data[i].check = true;
            }
        }
        else if (a  === false)
        {
            for (let i = 0; i < data.length; i++)
            {
                data[i].check = false;
            }
        }
        else
        {
            alert("my bad, sorry");
        }

        out();
        checkbox();
        pagination();
    });
    $(document).on('change',checkbox);

    // РЕДАКТИРОВАНИЕ
    $(document).on('dblclick', '.task-todo', function () {
        let todo = $(this);
        let inputEdit = $('.input-task-edit');
        const a = $(this).parent().attr('id');
        $('#myModal').modal("show");
        inputEdit.val(todo.text());
        $('.save-changes').on('click', ()=> {
            data.forEach((item, index)=> {
                if (todo.text() === item.todo)
                {
                    if (+a === +item.id)
                    {
                        let x = htmlspecialchars(inputEdit.val().trim());
                        item.todo = x || todo.text();

                    }
                }
            });

            out();
            checkbox();
            pagination();
        });


        delete_add();
    });

    // ПОКАЗАТЬ ВСЕ, ЗАВЕРШЕННЫЕ, АКТИВНЫЕ
    $(document).on('click', '#show_all', function () {
        mycheck = 0 ;
        activePage = 0 ;

        out();
        checkbox();
        pagination();
    });
    $(document).on('click', '#show_true', function () {
        mycheck = 1;
        activePage = 0 ;
        out();
        checkbox();
        pagination();
    });
    $(document).on('click', '#show_false', function () {
        mycheck = 2;
        activePage = 0 ;
        out();
        checkbox();
        pagination();
    });

    // УДАЛИТЬ ВЫДЕЛЕННОЕ, ОДИН ТАСК
    $(document).on('click', '#del_true', function () {
        data =data.filter(item=>{
            return item.check===false;
        });
        out();
        if(data.length===0)
        {
            $('#choose_all').prop('checked',false);
        }
        countTasks();
    });
    $('#out_added').on('click', '.del-todo', function () {
        const a =  parseFloat($(this).parent().attr('id'));
        data.forEach((item, index)=> {
            if (a == item.id)
            {
                data.splice(index,1);
                out();
                if (dataPage.length ===0 )
                {
                    activePage -= 1;

                    out();
                    checkbox();
                    pagination();
                }
            }

            if ($.isEmptyObject(data))
            {
                $('#out_added').html('there will be tasks...');
            }
            else
            {
                out();
                checkbox();
                pagination();
            }
        });

        out();
        checkbox();
        pagination();
    });

    // АКТИВНОСТЬ ВКЛАДОК
    $('.switch').on('click',function activeTask(){
        $('.switch').css('backgroundColor','none');
        $('.switch').css('color', 'none');

        $(this).css('backgroundColor','grey' );
        $(this).css('color', 'white');
    });
    $('.hideshow').on('click',function activeTask(){
        $('.hideshow').css('backgroundColor','none');
        $('.hideshow').css('color', 'none');

        $(this).css('backgroundColor','grey' );
        $(this).css('color', 'white');
    });

    // ВВОД ПО ENTER
    $("#add_input").keypress(function (e) {
        if (e.which === 13) {
            $("#add_button").click();
            delete_add();
        }
    });
    $(".input-task-edit").keypress(function (e) {
        if (e.which === 13) {
            $(".save-changes").click();
        }
    });

    // СКРЫТЬ ПОКАЗАТЬ СОДЕРЖИМОЕ
    $('#hide').click(function() {
        $('#out_added').hide(1000);
    });
    $('#show').click(function() {
        $('#out_added').show(1000);
    });
});
