$(() => {
    let todoList = [];
    let mycheck = 0 ;
    let page = 0;
    let activePageNum = 0;
    let massivePage = [];


    // АКТИВНОСТЬ ВКЛАДОК
    $('.switch').on('click',function activeTask(){
        $('.switch').css('backgroundColor','none');
        $('.switch').css('color', 'none');

        $(this).css('backgroundColor','grey' );
        $(this).css('color', 'white');
    });

    function checkbox () {
        let k=0;
        const l =todoList.length;
        $('#choose_all').prop('checked',false);
        for(let i = 0; i< todoList.length; i++)
        {
            if (todoList[i].check ===true)
            {
                k ++;
            }
        }
        if (k === (todoList.length) && todoList.length !== 0 )
        {
            $('#choose_all').prop('checked',true);
        }
        else
        {
            $('#choose_all').prop('checked',false);
        }
    }

    // ВЫБОР БОЛЬШОГО ЧЕКБОКСА, КОГДА ВСЕ ВЫБРАНО
    $(document).on ('change',bigChekboks );
    function bigChekboks() {
        let k=0;
        const l =todoList.length;
        $('#choose_all').prop('checked',false);
        for(let i = 0; i< todoList.length; i++)
        {
            if (todoList[i].check ===true)
            {
                k ++;
            }
        }
        if (k === (todoList.length) && todoList.length !== 0 )
        {
            $('#choose_all').prop('checked',true);
        }
        else
        {
            $('#choose_all').prop('checked',false);
        }
    }

    // ДОБАВЛЕНИЕ НОВОГО
    $('#add_button').on('click', function () {
        let todo = htmlspecialchars($('#add_input').val());
        if (!todo || !todo.trim())
        {
            alert("you must solve real tasks...");
        }
        else
        {
            let temp = {};
            temp.todo = todo;
            temp.check = false;
            temp.id = Math.random();

            todoList.push(temp);

            out();
            delete_add();
            checkbox();
        }


        // localStorage.setItem('todo', JSON.stringify(todoList));
        // не робит
        // localStorage.setItem('check',  JSON.stringify(todoList));

    });

    // ВЫПОЛНЕНИЕ ТАСКА (checkbox)
    $('#out_added').on('change', '.check-todo', function () {
        const idForCheck = Number($(this).parent().attr('id'));
        todoList.forEach(item => {
            if (item.id === idForCheck)
            {
                item.check = !item.check;
            }
        });
        out(todoList);
        // localStorage.setItem('check', JSON.stringify(todoList));
    });

    // УДАЛЕНИЕ ОДНОГО ЭЛЕМЕНТА
    $('#out_added').on('click', '.del-todo', function () {
        const a =  parseFloat($(this).parent().attr('id'));
        todoList.forEach((item, index)=> {
            if (a == item.id)
            {
                todoList.splice(index,1);
            }

            if ($.isEmptyObject(todoList))
            {
                $('#out_added').html('there will be tasks...');
            }
            else
            {
                out(todoList);
            }
        })
    });

    // РЕДАКТИРОВАНИЕ ПО ДВОЙНОМУ НАЖАТИЮ
    $(document).on('dblclick', '.task-todo', function () {
        let todo = $(this);
        let inputEdit = $('.input-task-edit');
        const a = $(this).parent().attr('id');
        $('#myModal').modal("show");
        inputEdit.val(todo.text());
        $('.save-changes').on('click', ()=> {
            todoList.forEach((item, index)=> {
                if (todo.text() === item.todo)
                {
                    if (+a === +item.id)
                    {
                        let x = htmlspecialchars(inputEdit.val().trim());
                        item.todo = x || todo.text();
                        out(todoList);
                    }
                }
            })
        });
        delete_add();
    });

    function htmlspecialchars(html) {
        html = html.replace(/&/g, "&amp;");
        html = html.replace(/</g, "&lt;");
        html = html.replace(/>/g, "&gt;");
        html = html.replace(/"/g, "&quot;");
        return html;
    }


    // ПОКАЗАТЬ ВСЕ
    $(document).on('click', '#show_all', function () {
        mycheck = 0 ;
        activePageNum = 0 ;
        out(todoList);
    });

    // ВЫБРАТЬ ВСЕ
    $(document).on('change', '#choose_all', function () {
    let a = $(this).is(":checked") ;

        if (a === true)
        {
            for (let i = 0; i < todoList.length; i++)
            {
                todoList[i].check = true;
            }
        }
        else if (a  === false)
        {
            for (let i = 0; i < todoList.length; i++)
            {
                todoList[i].check = false;
            }
        }
        else
        {
            alert("my bad, sorry");
        }

        out(todoList);
    });

    // ПОКАЗАТЬ АКТИВНЫЕ
    $(document).on('click', '#show_true', function () {
        mycheck = 1;
        activePageNum = 0 ;
        out(todoList);

    });

    // ПОКАЗАТЬ ЗАВЕРШЕННЫЕ
    $(document).on('click', '#show_false', function () {
        mycheck = 2;
        activePageNum = 0 ;
        out(todoList);
    });

    // УДАЛЕНИЕ ОТМЕЧЕННЫХ
    $(document).on('click', '#del_true', function () {
        todoList =todoList.filter(item=>{
            return item.check===false;
        });
        out(todoList);
         if(todoList.length===0)
         {
                 $('#choose_all').prop('checked',false);
         }
        countTaska(todoList);
    });

    // СКРЫТЬ ПОКАЗАТЬ СОДЕРЖИМОЕ
    $('#hide').click(function() {
        $('#out_added').hide(1000);
    });
    $('#show').click(function() {
        $('#out_added').show(1000);
    });

    // OUT
    function out() {
        let todoList1 =[];

        if (mycheck === 1)
        {
            todoList1 =todoList.filter(item=>{
                return item.check===false;
            });
            massivePage = [];
            page = 0;
        }
        else if (mycheck === 2 )
        {
            todoList1 =todoList.filter(item=>{
                return item.check===true;
            });
            massivePage = [];
            page = 0;
        }
        else
        {
            todoList1 = todoList;
        }

        if(activePageNum===0) {
            render(todoList1);
        }
        pag(todoList1);


        countTaska(todoList);
        if($.isEmptyObject(todoList1))
        {
            $('.out_added').html('there will be tasks ...');
            $('.pageActive').html('');
        }
    }

    // СЧЕТЧИК ТАСКОВ
    function countTaska(todoList123) {

        const all_task = todoList123.length;

        let false_task = 0;
        let true_task = 0;

        if ((!($.isEmptyObject(todoList123))) || todoList123 !== [] || todoList123 !== undefined || todoList123 !== null )
        {
            for (let i = 0; i < all_task; i++)
            {
                if (todoList123[i].check === false)
                {
                    false_task++;
                }
                if (todoList123[i].check === true)
                {
                    true_task++;
                }

                // счетчик выполненных задач
                if (false_task === undefined || false_task === 0 || false_task === null || (true_task === all_task && (all_task !== 0 || all_task !== [] || all_task !== null)))
                {
                    $("#false_task").html('');
                    $("#true_task").html('');
                    $("#massege_task").html('great! you made ' + all_task +' tasks');
                }
                else if (false_task === all_task || true_task === undefined || true_task === 0 || true_task === null)
                {
                    $("#false_task").html('');
                    $("#true_task").html('');
                    $("#massege_task").html('not good...active ' + all_task+'tasks');
                }
                else
                {
                    $("#false_task").html(false_task + '/' + all_task);
                    $("#true_task").html(true_task + '/' + all_task);
                    $("#massege_task").html('');
                }

            }

        }

    }

    // ВЫВОД
    function render(todoList1230) {

        let out = '';
        $("#out_added").empty();
        $("#massege_task").empty();

        todoList1230.forEach((item,index)=>{


        const idTodo = item.id;

            out = out + '<hr>' + `<span id="${idTodo}"><input class="check-todo " type="checkbox" ${item.check ? 'checked' : ''}>` + '       ';
            out = ''+out + '<a class="task-todo">'+item.todo +'</a>'+ '</>' + `<span id="${item.id}+todo"><button type="button" class="close del-todo" aria-label="Close" " >\n <span aria-hidden="true">×</span>\n` +
                '</button></span>' + '<hr>';

            if(index<5) {
                $("#out_added").html(out);
            }
        })

    }


    // ПАГИНАЦИЯ
    function pag (todoList123){
        let temp =[];
        let all_task = todoList123.length;
        const taskOnPage = 5;
        let countPage = Math.ceil((all_task/taskOnPage));
            if($('#add_button').on('click' ))
            {
                if (page !== countPage )
                {
                    massivePage=[];
                    page = countPage;
                    for( let i =0 ; i < page; i++)
                    {
                        massivePage[i]=i+1;
                    }
                    $('#pagination').html('');

                    massivePage.map(i => {
                        $('#pagination').append('<a class="pageActive" id="'+i+'"> '+ i+'</a>');
                    })
                }
            }


            // $('.pageActive').on('click',function(){
            //     for( let i = 0;i  < taskOnPage; i++){
            //         // console.log(this.id)
            //         if ((this.id)) {
            //
            //             for (let j=0; j < taskOnPage; j++)
            //             {
            //                 temp.push(todoList123[((this.id*taskOnPage)-taskOnPage + j)]);
            //             }
            //
            //             // out(temp);
            //             render(temp);
            //
            //         }
            //
            //     }
            // });



// ЭТО ГОВНО
//         $(document).on('click',arrForPage2 );
//
//         function arrForPage2() {
//             // console.log('activePageNum',activePageNum)
//
//             if(activePageNum !== 0 ) {
//                 console.log('activePageNum',activePageNum)
// //входит  НО НЕ ОТРИСОВЫВАЕТ ЗАНОВО
// // if (!this.id) {alert('nooooo')}
// //         activePageNum = this.id;
//                 let temp = [];
//
//                 for (let i = 0; i < taskOnPage; i++) {
//                     // console.log(this.id)
//                     // if ((this.id)) {
//                         // activePageNum = this.id;
//                         let x = todoList[((activePageNum * taskOnPage) - taskOnPage + i)];
// // выводит циклом до 5 элментов
//                         if (x) temp.push(x);
//
//                         // out(temp);
//                         render(temp);
//                         // console.log(temp)
//
//                     // }
//
//                 }
//             }
//
//         }

        // $(document).on('change', function () {
        //     $(`.pageActive#${activePageNum}`).click();
        // });
        $('#add_button').on('click', function () {
            $(`.pageActive#${activePageNum}`).click();
        });
        $('.del-todo').on('click', function () {
            $(`.pageActive#${activePageNum}`).click();
        });
        $('#choose_all').click( function () {
//             // $(`.pageActive#${activePageNum}`).click();
// checkbox();
        });



      $('.pageActive').on('click',arrForPage );
        function arrForPage() {
            activePageNum = this.id;

            let temp = [];

            for (let i = 0; i < taskOnPage; i++)
            {
                if ((this.id))
                {
                    activePageNum = this.id;
                    let x = todoList123[((this.id * taskOnPage) - taskOnPage + i)];
                    if (x) temp.push(x);
                    render(temp);
                }
            }
        }

        // активность страниц
        $('.pageActive#1').css('backgroundColor','grey');
        $('.pageActive#1').css('color', 'white');

        $('.pageActive').on('click',function(){
                $('.pageActive').css('backgroundColor','none');
                $('.pageActive').css('color', 'none');

                $(this).css('backgroundColor','#000' );
                $(this).css('color', 'white');
            });
        // }


    }

});


function delete_add() {
    // document.getElementById("add_input").value = null;
    $("#add_input").val(null);

}

$("#add_input").keypress(function (e) {
    if (e.which === 13) {
        $("#add_button").click();
        delete_add();
    }
});
$(".input-task-edit").keypress(function (e) {
    if (e.which === 13) {
        $(".save-changes").click();
    }
});

